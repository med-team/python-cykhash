Source: python-cykhash
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               cython3,
               python3-all-dev,
               python3-setuptools,
               python3-pytest <!nocheck>,
               python3-numpy <!nocheck>
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/python-cykhash
Vcs-Git: https://salsa.debian.org/med-team/python-cykhash.git
Homepage: https://github.com/realead/cykhash
Rules-Requires-Root: no

Package: python3-cykhash
Architecture: any
Depends: ${python3:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Description: cython wrapper for khash-sets/maps, efficient implementation of isin and unique
 Cykhash is a cython wrapper for khash-sets/maps, efficient
 implementation of isin and unique.
 .
   * Brings functionality of khash to Python and Cython and can be used
     seamlessly in numpy or pandas.
   * Numpy's world is lacking the concept of a (hash-)set. This
     shortcoming is fixed and efficient (memory- and speedwise compared
     to pandas') unique and isin are implemented.
   * Python-set/dict have big memory-footprint. For some datatypes the
     overhead can be reduced by using khash by factor 4-8.
